﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxHandler : MonoBehaviour
{
    public ObjectCache engineRespondeSfxCache;
    public ObjectCache footstepSfxCache;
    private List<AudioSource> spawnedSfx;

    private void OnEnable()
    {
        if (spawnedSfx == null)
        {
            spawnedSfx = new List<AudioSource>();
        }
        else
        {
            foreach (var sfx in spawnedSfx)
            {
                Destroy(sfx.gameObject);
            }

            spawnedSfx.Clear();
        }
    }

    private void FixedUpdate()
    {
        if (spawnedSfx.Count < 1) return;

        List<GameObject> sfxToRemove = new List<GameObject>();
        foreach (var sfx in spawnedSfx)
        {
            if (!sfx.isPlaying)
            {
                sfxToRemove.Add(sfx.gameObject);
                Destroy(sfx.gameObject);
            }
        }

        foreach (var sfx in sfxToRemove)
        {
            spawnedSfx.Remove(sfx.GetComponent<AudioSource>());
        }
    }

    public void PlayEngineResponseSfx()
    {
        engineRespondeSfxCache.GetNextInstance().SetActive(true);
    }

    public void PlayFootstepSfx()
    {
        footstepSfxCache.GetNextInstance().SetActive(true);
    }
}
