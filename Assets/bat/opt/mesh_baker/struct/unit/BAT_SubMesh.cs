﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace bat.opt.bake.util
{
    public class BAT_SubMesh
    {
        public int m_hashID;
        public Mesh m_mesh;
        public int m_subMeshID;
    }
}
