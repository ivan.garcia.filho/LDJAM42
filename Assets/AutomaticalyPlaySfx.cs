﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticalyPlaySfx : MonoBehaviour
{
    public bool oneshot = true;
    private bool alreadyExecuted = false;

    private void OnEnable()
    {
        alreadyExecuted = false;
    }

    private void LateUpdate()
    {
        if (alreadyExecuted && oneshot) return;

        var audioEmitters = GetComponents<AudioSource>();
        foreach (var audioEmitter in audioEmitters)
        {
            if (!audioEmitter.isPlaying)
            {
                audioEmitter.Play();
            }
        }

        alreadyExecuted = true;
    }
}
