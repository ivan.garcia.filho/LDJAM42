﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FireEventOnTriggerExit : MonoBehaviour
{
    public UnityEvent triggeredEvents;

    private void OnTriggerExit(Collider other)
    {
        triggeredEvents.Invoke();
    }
}
