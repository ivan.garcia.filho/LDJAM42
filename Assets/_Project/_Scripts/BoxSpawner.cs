﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGroup
{
    public List<GameObject> instances;
    public int curr_index;
}

public class BoxSpawner : MonoBehaviour
{
    public static List<Color> boxes_colors = new List<Color>();

    public ObjectCache dataPackageCache;
    public Transform begin;
    public Transform end;
    public float spawnChance = 66.666f;
    public Vector2 initialDelayRange = new Vector2(1, 9);
    private float startTime;
    public Vector2 spawnInterval;
    public Vector2 randomForce;
    public float throwAtDistance = 8f;
    public float packageSpeed = 15f;
    private float nextPackageSpawn;
    private List<Rigidbody> packages;
    public ParticleSystem itsIncomingPackageVfx;
    public Color box_color;
    public Color diffuse_scattering_color;

    float rot_y;

    private void Awake()
    {
        if (packages  == null)
        {
            packages = new List<Rigidbody>();
        }

        packages.Clear();

        startTime = Time.time + Random.Range(initialDelayRange.x,initialDelayRange.y);

        rot_y = transform.rotation.eulerAngles.y;

        boxes_colors.Add(diffuse_scattering_color);
    }

    private void FixedUpdate()
    {
        if (Game_Manager.self.state == Game_Manager.Game_State.MENU) return;

        var currentTime = Time.time;
        if (currentTime < startTime) return;

        MovePackages();

        if (currentTime < nextPackageSpawn) return;
        nextPackageSpawn = currentTime + Random.Range(spawnInterval.x,spawnInterval.y);

        if (Random.Range(0f, 100f) > spawnChance) return;

        SpawnNewPackage();
    }

    private void MovePackages()
    {
        var packagesToRemove = new List<Rigidbody>();
        foreach (var package in packages)
        {
            if (Vector3.Distance(package.transform.position, begin.position) > throwAtDistance)
            {
                package.constraints = RigidbodyConstraints.None;
                package.AddForce((end.forward * Random.Range(randomForce.x, randomForce.y)), ForceMode.Impulse);
                packagesToRemove.Add(package);
            }
            else
            {
                package.MoveByForcePushing_WithPhysics(end.forward, packageSpeed);
            }
        }

        foreach (var package in packagesToRemove)
        {
            packages.Remove(package);
        }
    }

    private void SpawnNewPackage()
    {
        var newPackage = dataPackageCache.GetFirstDisabledInstance();
        newPackage.transform.position = begin.position;
        newPackage.transform.rotation = Quaternion.identity;

        Material mat = newPackage.GetComponent<MeshRenderer>().material;
        mat.SetColor("_Color", box_color);
        mat.SetColor("_DiffuseScatteringColor", diffuse_scattering_color);

        var rb = newPackage.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = true;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        rb.gameObject.SetActive(true);

        packages.Add(rb);

        GameObject incoming_vfx = itsIncomingPackageVfx.gameObject;
        Transform incoming_vfx_t = incoming_vfx.transform;
        Quaternion q = incoming_vfx_t.rotation;
        Vector3 euler_q = q.eulerAngles;
        euler_q.y = rot_y + 90.0f;
        q.eulerAngles = euler_q;
        incoming_vfx_t.rotation = q;

        Vector3 p = begin.position;
        p.y = incoming_vfx_t.position.y;
        incoming_vfx_t.position = p;

        itsIncomingPackageVfx.Emit(1);
    }
}
