﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class ObjectCache : MonoBehaviour
{
	public GameObject prefab;
	public int prefabCount = 5;

	private GameObject[] instances;

	private int nextIndexToReturn = 0;

	private void Awake()
	{
		instances = new GameObject[prefabCount];

		for (int i = 0; i < prefabCount; i++)
		{
			instances[i] = Instantiate(prefab);
			instances[i].SetActive(false);
		}
	}

	private void OnEnable()
	{
		if (prefabCount < 1)
		{
			Debug.Log("Nothing To Instantiate");
			DestroyImmediate(this);
		}
	}

	public GameObject GetNextInstance()
	{
		if (nextIndexToReturn >= instances.Length) nextIndexToReturn = 0;

		var objToReturn = instances[nextIndexToReturn];
		objToReturn.SetActive(false);

		nextIndexToReturn++;
		return objToReturn;
	}

	public GameObject GetFirstDisabledInstance()
	{
		var objToReturn = instances.FirstOrDefault( _instance => _instance.activeSelf == false);

		return objToReturn == null? GetFirstEnabledInstance() : objToReturn;
	}

	public GameObject GetFirstEnabledInstance()
	{
		var objToReturn = instances.FirstOrDefault(_instance => _instance.activeSelf);

		return objToReturn == null ? GetFirstDisabledInstance() : objToReturn;
	}
}