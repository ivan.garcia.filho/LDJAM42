﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWaypoint : MonoBehaviour
{
    private List<Vector3> waypoints;
    public float loopSpeed = 9f;

    private void OnEnable()
    {
        if (waypoints == null)
        {
            waypoints = new List<Vector3>();
        }
        else
        {
            waypoints.Clear();
        }
    }

    public void AddWaypoint(Vector3 _point)
    {
        waypoints.Add(_point);
    }


    private int _currentDestination;
    public void LoopThroughWaypoints()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[_currentDestination], Time.smoothDeltaTime * loopSpeed);
    }
}
