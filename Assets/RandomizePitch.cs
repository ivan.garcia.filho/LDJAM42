﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizePitch : MonoBehaviour
{
    private void OnEnable()
    {
        var audioEmitters = GetComponents<AudioSource>();
        foreach (var audioEmitter in audioEmitters)
        {
            audioEmitter.pitch = Random.Range(0.9f,1.1f);
        }
    }
}
