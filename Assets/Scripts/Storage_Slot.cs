﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage_Slot : MonoBehaviour
{
    Vector3[] slots_positions;
    int slots_count = 0;

    List<GameObject> boxes = new List<GameObject>();
    public Tracker_Signal tracker;
    public ObjectCache cache, data_package_cache;

    bool stop_clear = false;

    void Awake()
    {
        slots_positions = new Vector3[3 * 8];

        float x = transform.position.x - 5.0f;
        float y = transform.position.y - 3.5f;

        int c = 0;
        Vector3 p = new Vector3();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                p.x = (x + j * 1.5f);
                p.y = (y + i * 1.5f);
                p.z = transform.position.z;

                slots_positions[c++] = p;
            }
        }
    }

    public int empty_storage_and_get_amount()
    {
        int result = boxes.Count;

        stop_clear = true;
        slots_count = 0;
        older_objs.Clear();
        older_objs.AddRange(boxes);
        boxes.Clear();
        StartCoroutine(clear_slots(older_objs));

        return result;
    }

    Vector3 unit_scale = new Vector3(1.0f, 1.0f, 1.0f);
    List<GameObject> older_objs = new List<GameObject>();
    public bool try_insert_item(Transform t)
    {
        if (slots_count >= slots_positions.Length)
            Game_Manager.self.reset_timer_and_score(empty_storage_and_get_amount());
        

        var vfx = Game_Manager.self.vfx_data_stored.GetNextInstance();

        vfx.transform.position = t.position;
        t.gameObject.SetActive(false);
        vfx.SetActive(true);

        StartCoroutine(disable_obj_delay(vfx, 1.5f));

        Material mat_from = t.gameObject.GetComponent<MeshRenderer>().material;

        GameObject box_obj = cache.GetNextInstance();
        Material mat_to = box_obj.GetComponent<MeshRenderer>().material;
        mat_to.SetColor("_Color", mat_from.GetColor("_Color"));
        mat_to.SetColor("_DiffuseScatteringColor", mat_from.GetColor("_DiffuseScatteringColor"));

        Vector3 next_p = slots_positions[slots_count++];
        box_obj.transform.position = next_p;
        box_obj.transform.rotation = Quaternion.identity;
        box_obj.transform.localScale = unit_scale;

        boxes.Add(box_obj);

        box_obj.SetActive(true);

        return true;
    }

    public Transform get_data()
    {
        if (boxes.Count <= 0) return null;

        boxes[boxes.Count - 1].gameObject.SetActive(false);
        boxes.RemoveAt(boxes.Count - 1);

        var p = data_package_cache.GetNextInstance();
        p.SetActive(true);

        return p.transform;
    }

    IEnumerator disable_obj_delay(GameObject obj, float sec)
    {
        yield return new WaitForSeconds(sec);
        obj.SetActive(false);
    }

    IEnumerator clear_slots(List<GameObject> objs)
    {
        bool has_box_visible = true;
        stop_clear = false;

        while (has_box_visible && !stop_clear)
        {
            has_box_visible = false;

            for (int i = 0; i < objs.Count; i++)
            {
                Transform t = objs[i].transform;
                Vector3 scale = t.localScale;

                if (scale.x > 0.1f) has_box_visible = true;

                scale.x = Mathf.Lerp(scale.x, 0, Time.smoothDeltaTime * ((i + 1) * 0.85f));
                scale.y = scale.z = scale.x;

                t.localScale = scale;
            }

            yield return Game_Manager.eof;
        }

        for (int i = 0; i < objs.Count; i++)
        {
            objs[i].SetActive(false);
        }

        stop_clear = false;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
   
    //}

    private void OnDrawGizmos()
    {
        float x = transform.position.x - 5.0f;
        float y = transform.position.y - 3.5f;

        Vector3 p = new Vector3();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                p.x = (x + j * 1.5f);
                p.y = (y + i * 1.5f);
                p.z = transform.position.z;

                Gizmos.DrawWireCube(p, Vector3.one * 0.9f);
            }
        }
    }
}
