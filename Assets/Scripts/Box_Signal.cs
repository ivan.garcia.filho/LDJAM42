﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Box_Signal : MonoBehaviour
{
    Transform t;
    BoxCollider coll;
    Rigidbody rb;
    NavMeshObstacle na;

    public GameObject impactSfxPrefab;

    private void Awake()
    {
        t = transform;
        coll = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();
        na = GetComponent<NavMeshObstacle>();

        t.localScale = Vector3.one * Random.Range(0.68f, 1.2f);
    }

    private void OnEnable()
    {
        //secs_until_fade_away = 15.0f;
        if(na) na.enabled = true;
    }

    private void OnCollisionEnter(Collision collision)
    {   
        if (rb.useGravity && rb.velocity.magnitude > 1f)
        {
            var sfx_impact = Game_Manager.self.sfx_box_impact.GetNextInstance();
            sfx_impact.SetActive(true);

            // I know its fucking ugly but thats what we have now dont cry over it
            AudioSource audio_src = sfx_impact.GetComponent<AudioSource>();
            audio_src.Stop();
            audio_src.Play();
        }
    }

    public void got_owned()
    {
        coll.enabled = false;
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.mass = 0;
        rb.angularDrag = 0;
        rb.drag = 0;

        na.enabled = false;
    }
}
