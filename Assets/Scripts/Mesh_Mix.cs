﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR 
using UnityEditor;
#endif

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Mesh_Mix : MonoBehaviour
{
    public MeshFilter[] meshes;

    public void mix_meshes()
    {
        MeshFilter mf = GetComponent<MeshFilter>();

        CombineInstance[] combine = new CombineInstance[meshes.Length];
        for (int i = 0; i < meshes.Length; i++)
        {
            combine[i].mesh = meshes[i].sharedMesh;
            combine[i].transform = meshes[i].transform.localToWorldMatrix;

            meshes[i].gameObject.SetActive(false);
        }

        mf.sharedMesh = new Mesh();
        mf.sharedMesh.CombineMeshes(combine);
    }
}

#if UNITY_EDITOR 
[CustomEditor(typeof(Mesh_Mix))]
public class Mesh_Mix_Editor : Editor
{
    Mesh_Mix mm;

    private void OnEnable()
    {
        mm = (Mesh_Mix)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Mix"))
        {
            mm.mix_meshes();
        }
    }
}

#endif