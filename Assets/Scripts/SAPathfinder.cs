﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PathRequest
{
    public Vector3 from, to;

    public delegate void onFindPath(List<Vector3> path);
    public onFindPath callback;
}

struct Node
{
    public float x;
    public float y;
    public float p_x;
    public float p_y;
    public byte available;
}

public class SAPathfinder : MonoBehaviour
{
    public string memory_info;

    public int width = 10, height = 10;

    float[,] node_x;
    float[,] node_y;
    int[,] node_parent_x;
    int[,] node_parent_y;
    byte[,] node_available;

    void Awake()
    {
        node_x = new float[width, height];
        node_y = new float[width, height];
        node_parent_x = new int[width, height];
        node_parent_y = new int[width, height];
        node_available = new byte[width, height];

        calculate_mem_size(node_x.Length);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                node_x[x, y] = x;
                node_y[x, y] = y;
                node_parent_x[x, y] = -1;
                node_parent_y[x, y] = -1;

                node_available[x, y] = 0;
            }
        }
    }

    void Update()
    {

    }

    private void OnValidate()
    {
        int length = width * height;
        calculate_mem_size(length);
    }

    void calculate_mem_size(int l)
    {
        float floats_arrays_mem_size = ((float)l * (float)(4.0f * 0.000001f)) * 4;
        float byte_array_mem_size = ((float)l * (float)(1.0f * 0.000001f));

        memory_info = "Nodes data: " + (floats_arrays_mem_size + byte_array_mem_size) + "mb";
    }
}
