﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box_Deactivator : MonoBehaviour
{
    public LayerMask box_layer;

    private void OnTriggerExit(Collider other)
    {
        GameObject obj = other.gameObject;
        int layer = obj.layer;
        if(Game_Manager.layer_is(layer, box_layer))
            obj.SetActive(false);      
    }

}
