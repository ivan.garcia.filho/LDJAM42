﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deliver_Tracker : MonoBehaviour
{
    public Storage_Slot storage;

    private void OnCollisionStay(Collision collision)
    {
        Game_Manager.self.tracker_collided_with(collision.transform, storage);
    }
}
