﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker_Signal : MonoBehaviour
{
    Transform t;
    public Transform begin_t, end_t;

    List<Transform> boxes = new List<Transform>();
    public Storage_Slot storage;
    Vector3 dir;

    public LayerMask box_layer;

    private void Awake()
    {
        t = transform;

        dir = end_t.position - begin_t.position;
    }

    private void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(Game_Manager.layer_is(collision.transform.gameObject.layer, box_layer))
            storage.try_insert_item(collision.transform);
        
    }
}
