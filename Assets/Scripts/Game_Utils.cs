﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Utils
{
    static Texture2D white_texture;
    public static Texture2D white_texture_s
    {
        get
        {
            if (white_texture == null)
            {
                white_texture = new Texture2D(1, 1);
                white_texture.SetPixel(0, 0, Color.white);
                white_texture.Apply();
            }

            return white_texture;
        }
    }

    public static void draw_screen_rect(Rect rect, Color color)
    {
        GUI.color = color;
        GUI.DrawTexture(rect, white_texture_s);
        GUI.color = Color.white;
    }

    public static void draw_screen_rect_border(Rect rect, float thickness, Color color)
    {
        // Top
        draw_screen_rect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
        // Left
        draw_screen_rect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
        // Right
        draw_screen_rect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
        // Bottom
        draw_screen_rect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
    }

    public static Rect get_screen_rect(Vector3 screen_position1, Vector3 screen_position2)
    {
        // Move origin from bottom left to top left
        screen_position1.y = Screen.height - screen_position1.y;
        screen_position2.y = Screen.height - screen_position2.y;
        // Calculate corners
        var topLeft = Vector3.Min(screen_position1, screen_position2);
        var bottomRight = Vector3.Max(screen_position1, screen_position2);
        // Create Rect
        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public static Bounds get_viewport_bounds(Camera camera, Vector3 screen_position1, Vector3 screen_position2)
    {
        var v1 = camera.ScreenToViewportPoint(screen_position1);
        var v2 = camera.ScreenToViewportPoint(screen_position2);
        var min = Vector3.Min(v1, v2);
        var max = Vector3.Max(v1, v2);
        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;

        var bounds = new Bounds();
        bounds.SetMinMax(min, max);
        return bounds;
    }
}
