﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public struct Bot_Agent
{
    public Transform box_t;
    public Animator anmt;
    public NavMeshAgent nm_agent;
    public TextMeshPro flag_text;

    public List<Vector3> marked_points;
    public Vector3 dest, dir;
    public Vector3 prev_pos;

    public bool moving, has_box;
    public int curr_point;
    public float seconds_stucked;
}

public class Game_Manager : MonoBehaviour
{
    public static Game_Manager self;
    public enum Game_State { MENU, GAMEPLAY }
    [HideInInspector]
    public Game_State state = Game_State.MENU;

    public Camera cam;
    public Transform cam_parent;

    public GameObject selected_bot_highlight;
    public ObjectCache set_focus_cache, set_dest_cache, sfx_box_impact, vfx_data_stored;
    public Transform loop_vfx;
    public Storage_Slot storage_slot;

    public float bots_speed = 35.0f;
    public GameObject[] bot_agents;
    public Transform[] bot_agents_t;
    Dictionary<Transform, Bot_Agent> bot_agents_data = new Dictionary<Transform, Bot_Agent>();

    public LayerMask interactable_objs_layer;
    public LayerMask ground_layer, bots_layer;

    public Transform car_start, car_point;
    public Transform[] cars;
    int car_index = 0;
    bool car_get_boxes = true;
    float secs_waiting_elapsed = 0.0f;
    float secs_waiting = 15.0f;
    List<Transform> moving_boxes = new List<Transform>();

    List<Transform> selected_bots = new List<Transform>();

    bool creating_bot_path = false;

    public static WaitForEndOfFrame eof = new WaitForEndOfFrame();

    public TextMeshProUGUI money_text;
    public TextMeshPro timer_text;
    public GameObject game_over_title;
    public GameObject[] selected_bot_vfxs = new GameObject[3];
    Transform[] selected_bot_vfxs_t = new Transform[3];
    int money = 0;
    int boxes_count = 0;
    float min_elapsed = 0.0f;
    float timer_secs = 30.0f;
    bool take_screenshot = false;

    bool is_mouse_selecting = false;
    Vector3 mouse_pos1;

    public Toggle fullscreen_toggle;

    void Awake ()
    {
        self = this;

        bot_agents_t = new Transform[bot_agents.Length];

        for (int i = 0; i < bot_agents.Length; i++)
        {
            GameObject obj = bot_agents[i];

            Bot_Agent ba = new Bot_Agent();
            ba.moving = false;
            ba.marked_points = new List<Vector3>();
            ba.anmt = obj.GetComponentInChildren<Animator>();
            ba.nm_agent = obj.GetComponent<NavMeshAgent>();
            ba.nm_agent.updateRotation = true;
            ba.prev_pos = obj.transform.position;

            ba.flag_text = obj.GetComponentInChildren<TextMeshPro>();
            ba.flag_text.text = (i + 1).ToString();

            bot_agents_t[i] = obj.transform;
            bot_agents_data.Add(bot_agents_t[i], ba);
        }

        for (int i = 0; i < selected_bot_vfxs.Length; i++)
            selected_bot_vfxs_t[i] = selected_bot_vfxs[i].transform;


#if UNITY_EDITOR
        StartCoroutine(check_screenshots());
#endif
    }

    public void reset_timer_and_score(int amnt)
    {
        money_text.text = "POINTS: " + (amnt).ToString();
        timer_secs = 30.0f;
    }

    void Update()
    {

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F4))
        {
            Debug.Log("HEY!");
            take_screenshot = true;
        }
#endif

        if(Input.GetKeyDown(KeyCode.Escape))
            fullscreen_toggle.isOn = false;

        float dt = Time.deltaTime;

        if (state == Game_State.GAMEPLAY)
        {
            timer_secs -= dt;
            if (timer_secs <= 0.0f)
            {
                int boxes_amount = storage_slot.empty_storage_and_get_amount();
                reset_timer_and_score(boxes_amount);
            }

            timer_text.text = timer_secs.ToString("0");

            handle_user_input();
            simulate_bots(dt);
            MoveWaypointConnectionLineVfxTowardsCurrentDestination();
        }
        else
        {
            is_mouse_selecting = false;
        }
    }

    private Vector3 currentDestination;
    private void MoveWaypointConnectionLineVfxTowardsCurrentDestination()
    {
        loop_vfx.position = Vector3.MoveTowards(loop_vfx.position, currentDestination, Time.smoothDeltaTime * 18f);
    }

    void handle_user_input()
    {
        bool left_btn = Input.GetMouseButtonDown(0);
        bool right_btn = Input.GetMouseButtonDown(1);
        bool shift = Input.GetKey(KeyCode.LeftShift);

        bool left_btn_up = Input.GetMouseButtonUp(0);

        if(left_btn)
        {
            selected_bots.Clear();

            is_mouse_selecting = true;
            mouse_pos1 = Input.mousePosition;
        }
        if (left_btn_up)
            is_mouse_selecting = false;

        if (left_btn || right_btn)
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000, interactable_objs_layer))
            {
                Transform obj_t = hit.transform;
                int layer = obj_t.gameObject.layer;
                if (layer_is(layer, bots_layer))
                {
                    if (left_btn)
                    {
                        selected_bots.Clear();
                        select_bot(obj_t);
                    }
                }
                else // if its not bot, then its other interactable object, then the bot should go there
                {
                    Vector3 p = hit.point;
                    var selected_bots_count = selected_bots.Count;
                    if (selected_bots_count > 0)
                    {
                        for (int i = 0; i < selected_bots_count; i++)
                        {
                            Transform bot_t = selected_bots[i];
                            Bot_Agent ba = bot_agents_data[bot_t];

                            p.y = bot_t.position.y;
                            if (right_btn)
                            {
                                if (!shift)
                                    ba.marked_points.Clear();

                                ba.marked_points.Add(p);
                                ba.moving = true;

                                show_fx(set_dest_cache, p);

                                loop_vfx.gameObject.SetActive(true);
                                currentDestination = p;
                                //StartCoroutine(move_loop_to(p));
                            }

                            bot_agents_data[bot_t] = ba;
#if UNITY_EDITOR
                            Debug.Log("Sending selected bots to " + p);
#endif
                        }
                    }
                }
            }
        }
    }

    Vector3 vzero = Vector3.zero;
    Transform bot_t = null;
    private int speedParamHash = Animator.StringToHash("speed");
    void simulate_bots(float dt)
    {
        var amountOfBotAgents = bot_agents.Length;
        for (int i = 0; i < amountOfBotAgents; i++)
        {
            bot_t = bot_agents_t[i];

            if(is_within_selection_bounds(bot_t.position))
                select_bot(bot_t);

            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                selected_bots.Clear();
                select_bot(bot_t);
            }

            if (selected_bots.Contains(bot_t))
            {
                var selected_bot_vfx = selected_bot_vfxs[i];
                var selected_bot_vfx_t = selected_bot_vfxs_t[i];

                if (!selected_bot_vfx.activeInHierarchy) selected_bot_vfx.SetActive(true);

                Vector3 p = bot_t.position;
                p.y = selected_bot_vfx_t.position.y;
                selected_bot_vfx_t.position = p;

                selected_bot_vfx_t.rotation = bot_t.rotation;
            }
            else
            {
                var selected_bot_vfx = selected_bot_vfxs[i];
                var selected_bot_vfx_t = selected_bot_vfxs_t[i];

                if (selected_bot_vfx.activeInHierarchy) selected_bot_vfx.SetActive(true);
            }

            Bot_Agent ba = bot_agents_data[bot_t];

            if (!ba.moving)
            {
                ba.anmt.SetFloat(speedParamHash, 0.0f);
                if(!ba.nm_agent.isStopped) ba.nm_agent.isStopped = true;
                continue;
            }

            ba.dest = ba.marked_points[0];
            ba.dir = ba.dest - bot_t.position;
            float mag = (ba.dir).magnitude;
            if(mag < 1.0f)
            {
                ba.marked_points.RemoveAt(0);
                if(ba.marked_points.Count == 0)
                {
                    ba.moving = false;
                    bot_agents_data[bot_t] = ba;
                    continue;
                }

                ba.dest = ba.marked_points[0];
            }


            ba.anmt.SetFloat(speedParamHash, 1.0f);

            Quaternion q = Quaternion.LookRotation(ba.dir);
            Vector3 ea = q.eulerAngles;
            ea.x = ea.z = 0.0f;
            q.eulerAngles = ea;

            if (ba.nm_agent.isStopped)
                ba.nm_agent.isStopped = false;

            ba.nm_agent.SetDestination(ba.dest);

            if ((ba.prev_pos - bot_t.position).magnitude <= 0.1f)
                ba.seconds_stucked += dt;
            else
            {
                ba.seconds_stucked = 0.0f;
                ba.flag_text.text = (i + 1).ToString();
            }

            if (ba.seconds_stucked >= 2.0f)
            {
                ba.flag_text.text = "!!!";
                ba.seconds_stucked = 0.0f;
            }

            ba.prev_pos = bot_t.position;

            bot_agents_data[bot_t] = ba;
        }
    }

    void move_boxes_on_track()
    {
        Transform car_t = cars[car_index];
        float car_dist = (car_point.position - car_t.position).magnitude;

        if (!car_get_boxes || car_dist > 0.8f) return;

        for (int i = 0; i < moving_boxes.Count; i++)
        {
            Transform t = moving_boxes[i];
            Vector3 cp = t.position;

            Vector3 compare_p = cp;
            compare_p.x = car_point.position.x;
            compare_p.y = car_point.position.y;
            if ((car_point.position - compare_p).magnitude < 0.5f)
            {
                Vector3 s = t.localScale;

                //if (s.x > 0.0f)
                //{
                //    s.x = Mathf.Lerp(s.x, 0.0f, Time.smoothDeltaTime);
                //    s.z = s.y = s.x;
                //    t.localScale = s;
                //}
                //else
                {
                    moving_boxes.Remove(t);
                   // t.gameObject.SetActive(false);

                    i -= 1;
                }
            }
            else
            {
               // cp.z = Mathf.Lerp(cp.z, car_point.position.z, Time.smoothDeltaTime);
                //t.position = cp;
            }
        }
    }

    void handle_cam_movement()
    {
        float sd = Input.mouseScrollDelta.y;
        float curr_ortho_size = cam.orthographicSize;
        curr_ortho_size -= sd;
        cam.orthographicSize = Mathf.Clamp(curr_ortho_size, 4, 14);

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        cam_parent.position += h * cam_parent.right + v * cam_parent.forward;
    }

    public void get_money(int v)
    {
        if (money >= v)
        {
            money -= v;
            money_text.text = "Points: " + money.ToString();
        }
        else
        {
            state = Game_State.MENU;

            for (int i = 0; i < bot_agents.Length; i++)
            {
                var bot_t = bot_agents_t[i];
                Bot_Agent ba = bot_agents_data[bot_t];
                ba.moving = false;
                ba.nm_agent.isStopped = true;
                bot_agents_data[bot_t] = ba;
            }

            game_over_title.SetActive(true);
            StartCoroutine(wait_and_restart());
        }
    }

    public bool tracker_collided_with(Transform t, Storage_Slot ss)
    {
        if (bot_agents_data.ContainsKey(t))
        {
            Bot_Agent ba = bot_agents_data[t];

            if (!ba.has_box)
            {
                Transform box_t = ss.get_data();
                if (box_t == null) return false;

                // set the box
                ba.has_box = true;
                ba.box_t = box_t;
                box_t.SetParent(t);

                // set box position to be on top of the bot
                Vector3 tp = t.position;
                tp.y += 1;
                box_t.position = tp;

                Box_Signal bs = box_t.gameObject.GetComponent<Box_Signal>();
                bs.got_owned();

                // update the data
                bot_agents_data[t] = ba;

                return true;
            }

            return false;
        }

        return false;
    }

    public Transform storage_collided_with(Transform t, Transform tracker_t)
    {
        if (bot_agents_data.ContainsKey(t))
        {
            Bot_Agent ba = bot_agents_data[t];

#if UNITY_EDITOR
            Debug.Log("Bot " + t.gameObject.name + " collided on track and it has box ? " + ba.has_box);
#endif

            if (ba.has_box)
            {
                ba.has_box = false;

                ba.box_t.SetParent(null);

                Transform box_t = ba.box_t;
                ba.box_t = null;

                // increase one box added
                boxes_count++;

                bot_agents_data[t] = ba;

                return box_t;
            }
        }

        return null;
    }

    void select_bot(Transform obj_t)
    {
        if (!selected_bots.Contains(obj_t) && bot_agents_data.ContainsKey(obj_t))
        {
            selected_bots.Add(obj_t);
            set_highlight(obj_t);

#if UNITY_EDITOR
            Debug.Log("Selected bot " + obj_t.gameObject);
#endif
        }
    }

    IEnumerator move_loop_to(Vector3 to)
    {
        float mag = (to - loop_vfx.position).magnitude;
        loop_vfx.gameObject.SetActive(true);
        var threshold = 0.7 * 0.7;

        while (mag > threshold)
        {
            loop_vfx.position = Vector3.MoveTowards(loop_vfx.position, to, Time.smoothDeltaTime * 18f);
            mag = (to - loop_vfx.position).sqrMagnitude;

            yield return eof;
        }

        loop_vfx.gameObject.SetActive(false);
    }

    public void set_highlight(Transform t)
    {
        loop_vfx.gameObject.SetActive(false);
        loop_vfx.position = t.position;

        show_fx(set_focus_cache, t.position);
    }

    IEnumerator wait_and_restart()
    {
        yield return new WaitForSeconds(4.2f);

        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void toggle_simulation()
    {
        state = (state == Game_State.GAMEPLAY) ? Game_State.MENU : Game_State.GAMEPLAY;
    }

    void show_fx(ObjectCache cache, Vector3 p)
    {
        GameObject obj = cache.GetNextInstance();
        obj.transform.position = p;
        obj.SetActive(true);
    }

    public void toggle_volume()
    {
        AudioListener.volume = (AudioListener.volume > 0.0f) ? 0.0f : 1.0f;
    }

    public void toggle_fullscreen()
    {
        if (Screen.fullScreen)
        {
            Screen.fullScreen = false;
            Screen.SetResolution(960, 540, false);
        }
        else
        {
            Screen.fullScreen = true;
        }
    }

    public static bool layer_is(int layer, LayerMask lm)
    {
        return (lm == (lm | (1 << layer)));
    }

    public bool is_within_selection_bounds(Vector3 p)
    {
        if (!is_mouse_selecting)
            return false;

        var viewport_bounds = Game_Utils.get_viewport_bounds(cam, mouse_pos1, Input.mousePosition);
        return viewport_bounds.Contains(cam.WorldToViewportPoint(p));
    }

    void OnGUI()
    {
        if (is_mouse_selecting)
        {
            var rect = Game_Utils.get_screen_rect(mouse_pos1, Input.mousePosition);

            Game_Utils.draw_screen_rect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Game_Utils.draw_screen_rect_border(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    private void OnDrawGizmos()
    {
        //if(selected_bots.co)
        //{
        //    Gizmos.color = Color.magenta;
        //    Gizmos.DrawWireSphere(selected_bot.position, 0.5f);

        //    if(bot_agents_data[selected_bot].moving)
        //    {
        //        Gizmos.color = Color.cyan;
        //        Gizmos.DrawLine(selected_bot.position, selected_bot.position + bot_agents_data[selected_bot].dir * 2);
        //    }
        //}
    }

#if UNITY_EDITOR
    IEnumerator check_screenshots()
    {
        while(Application.isPlaying)
        {
            yield return eof;
            if (take_screenshot)
            {
                ScreenCapture.CaptureScreenshot("Screenshots/screenshot_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".png");
                take_screenshot = false;

                // wait for half a second to take another
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
#endif
}
